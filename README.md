# web项目脚手架

#### 项目介绍
基于Filter拦截器实现的注解式的mvc请求处理
jdbc的简单封装

#### 软件架构
Servlet 3.0+jdbc+MySql+jsp+FreeMarker


#### 安装教程
1、新建web项目
2、导入sim.jar

#### 使用说明
详细使用请参考examples目录示例代码
1、请求方法的参数只能注入Model
2、jar依赖：
	Servlet-3.0.jar（maven工程）或tomcat7.0以上
	c3p0.jar
	fastjson.jar
	freemarker.jar 版本需要2.3.25或以上
	mysql-connector-java.jar

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)