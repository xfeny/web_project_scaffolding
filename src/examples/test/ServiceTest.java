package examples.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

import examples.model.User;
import examples.service.UserService;
import sim.core.dbrecord.DbUtils;
import sim.core.dbrecord.Page;
import sim.core.dbrecord.dataSoure.C3p0DataSource;
import sim.core.utils.PropUtils;

public class ServiceTest {

	private UserService userService;

	@Before
	public void initDb() {
		PropUtils.load("application");
		C3p0DataSource dataSource = new C3p0DataSource();
		dataSource.setJdbcUrl(PropUtils.getProp("database.url"));
		dataSource.setUser(PropUtils.getProp("database.username"));
		dataSource.setPassword(PropUtils.getProp("database.password"));
		dataSource.setDriverClass(PropUtils.getProp("database.driver"));
		dataSource.start();
		DbUtils.init(dataSource.getDataSource());
	}

	@Test
	public void insert() {
		User user = new User();
		user.setCreatTime(new Date());
		user.setName("fenyong");
		user.setPassword("123456");
		// 实体对象和数据库表名不一致
		DbUtils.insert(user, "user");
	}

	@Test
	public void insert2() {
		User user = new User();
		user.setCreatTime(new Date());
		user.setName("fenyong");
		user.setPassword("123456");
		// 实体对象和数据库表名一致
		DbUtils.insert(user);
	}

	@Test
	public void update() {
		User user = new User();
		user.setId("7");
		user.setCreatTime(new Date());
		user.setName("feny");
		user.setPassword("123456aaa");
		user.setCash(new BigDecimal("100.123"));
		DbUtils.update(user, "user");
	}

	@Test
	public void update2() {
		User user = new User();
		user.setId("20");
		user.setCreatTime(new Date());
		user.setName("feny");
		user.setPassword("abcdef");
		user.setCash(new BigDecimal("100.123"));
		DbUtils.update(user);
	}

	@Before
	public void getService() {
		setUserService(new UserService());
	}

	@Test
	public void queryList() {
		List<User> users = userService.queryForList();
		System.out.println(users);
	}

	@Test
	public void findById() {
		User user = userService.findById(5L);
		System.out.println(user);
	}

	@Test
	public void queryForPage() {
		Page page = new Page();
		page.setPage(2);
		page.setPageSize(2);
		page = userService.queryForPage(page, "f");
		System.out.println(JSONObject.toJSONString(page));
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
