package examples.test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import sim.core.consts.CompareEnum;
import sim.core.consts.OperatorEnum;
import sim.core.consts.OrderByEnum;
import sim.core.dbrecord.DbUtils;
import sim.core.dbrecord.SqlBuilder;

/**
 * 测试SQL生成工具类
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年6月8日上午11:19:42
 */
public class SqlBuilderTest {

	@Test
	public void insert() {
		SqlBuilder.begin();
		SqlBuilder.insert("user");
		SqlBuilder.values("name", "Feny20180607");
		SqlBuilder.values("cash", 123);
		// System.out.println(SqlBuilder.getSQL());
		// System.out.println(Arrays.toString(SqlBuilder.getParams()));

		DbUtils.insert(SqlBuilder.getSQL(), SqlBuilder.getParams());
	}

	@Test
	public void update() {
		SqlBuilder.begin();
		SqlBuilder.update("user");
		SqlBuilder.sets("name", "yong2018");
		SqlBuilder.sets("password", "20000608");
		SqlBuilder.where("id", 26);
		// System.out.println(SqlBuilder.getSQL());
		// System.out.println(Arrays.toString(SqlBuilder.getParams()));

		DbUtils.update(SqlBuilder.getSQL(), SqlBuilder.getParams());
	}

	@Test
	public void delete() {
		SqlBuilder.begin();
		SqlBuilder.delete("user");
		SqlBuilder.where("id", 1);
		System.out.println(SqlBuilder.getSQL());
		System.out.println(Arrays.toString(SqlBuilder.getParams()));

		// DbUtils.delete(SqlBuilder.getSQL(), SqlBuilder.getParams());
	}

	@Test
	public void select1() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		System.out.println(SqlBuilder.getSQL());
		System.out.println(Arrays.toString(SqlBuilder.getParams()));
	}

	@Test
	public void select2() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		SqlBuilder.where("cash IS NOT NULL");

		List<Map<String, Object>> lists = DbUtils.queryForList(SqlBuilder.getSQL(), SqlBuilder.getParams());
		System.out.println(lists);
	}

	@Test
	public void select3() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		SqlBuilder.where("cash", 10, CompareEnum.GT);
		SqlBuilder.where("cash", 20, CompareEnum.LT);
		List<Map<String, Object>> lists = DbUtils.queryForList(SqlBuilder.getSQL(), SqlBuilder.getParams());
		System.out.println(lists);
	}

	@Test
	public void select4() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		SqlBuilder.where("password", 123, null, OperatorEnum.OR);
		SqlBuilder.where("name", "Feny", CompareEnum.LIKE);

		List<Map<String, Object>> lists = DbUtils.queryForList(SqlBuilder.getSQL(), SqlBuilder.getParams());
		System.out.println(lists);
	}

	@Test
	public void select5() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		SqlBuilder.groupBy("password");
		// SqlBuilder.orderBy("id DESC");
		SqlBuilder.orderBy("id", OrderByEnum.DESC);

		List<Map<String, Object>> lists = DbUtils.queryForList(SqlBuilder.getSQL(), SqlBuilder.getParams());
		System.out.println(lists);
	}

	@Test
	public void select6() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		SqlBuilder.where("cash", new Object[] { 10, 20 }, CompareEnum.BETWEEN, OperatorEnum.OR);

		List<Map<String, Object>> lists = DbUtils.queryForList(SqlBuilder.getSQL(), SqlBuilder.getParams());
		System.out.println(lists);
	}

	@Test
	public void select7() {
		SqlBuilder.begin();
		SqlBuilder.select("*");
		SqlBuilder.from("user");
		// SqlBuilder.where("name", "Feny", CompareEnum.NOT_IN);
		// SqlBuilder.where("cash", "SELECT id FROM user WHERE cash > 10",
		// CompareEnum.NOT_IN);
		SqlBuilder.where("name", new Object[] { "许芬勇", "feny", "xufenyong" }, CompareEnum.IN);

		List<Map<String, Object>> lists = DbUtils.queryForList(SqlBuilder.getSQL(), SqlBuilder.getParams());
		System.out.println(lists);
	}
}
