package examples.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import examples.model.Article;
import examples.model.User;
import sim.core.dbrecord.DbUtils;
import sim.core.dbrecord.Tx;
import sim.core.dbrecord.dataSoure.DefaultDataSource;
import sim.core.dbrecord.dataSoure.DruidDataSource;
import sim.core.utils.PropUtils;

/**
 * 测试Dbutils
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年5月22日上午11:32:28
 */
public class JdbcTest {

	@Before
	public void initDb() {
		PropUtils.load("application");
		String jdbcUrl = PropUtils.getProp("database.url");
		String user = PropUtils.getProp("database.username");
		String password = PropUtils.getProp("database.password");
		String driverClass = PropUtils.getProp("database.driver");
		// DefaultDataSource dataSource = new C3p0DataSource(jdbcUrl, user,
		// password, driverClass);
		DefaultDataSource dataSource = new DruidDataSource(jdbcUrl, user, password, driverClass);
		dataSource.start();
		DbUtils.init(dataSource.getDataSource());
	}

	@Test
	public void insert() {
		User user = new User();
		user.setCreatTime(new Date());
		user.setName("fenyong");
		user.setPassword("123456");
		long id = DbUtils.insert(user, "user");
		System.out.println(id);
	}

	@Test
	public void update() {
		User user = new User();
		user.setId("21");
		user.setCreatTime(new Date());
		user.setName("xfeny");
		user.setPassword("12789");
		user.setCash(new BigDecimal("100.123"));
		DbUtils.update(user, "user");
	}

	@Test
	public void delect() {
		String sql = "DELETE FROM user WHERE id=?";
		Object[] params = { 65L };
		DbUtils.delete(sql, params);
	}

	@Test
	public void delect2() {
		Map<String, Object> params = new HashMap<>();
		params.put("password", "123456aaa");
		String tableName = "user";
		DbUtils.delete(tableName, params);
	}

	@Test
	public void count() {
		String sql = "SELECT id,name FROM user where age>28";
		long count = DbUtils.count(sql, null);
		System.out.println(count);
	}

	@Test
	public void queryForBean() {
		// String sql = "SELECT * FROM user WHERE id=5";
		// User user = DbUtils.queryForBean(sql, null, User.class);

		String sql = "SELECT * FROM user WHERE id=?";
		Object[] params = { 10 };
		User user = DbUtils.queryForBean(sql, params, User.class);
		System.out.println(user);
	}

	@Test
	public void getForBean1() {
		Map<String, Object> params = new HashMap<>();
		params.put("id", "21");
		User user = DbUtils.queryForBean(params, User.class);
		System.out.println(user);
	}

	@Test
	public void queryForMap() {
		String sql = "SELECT * FROM user WHERE id=?";
		Object[] params = { 10 };
		Map<String, Object> map = DbUtils.queryForMap(sql, params);
		System.out.println(map);
	}

	@Test
	public void getListBean() {
		String sql = "SELECT * FROM user";
		List<User> list = DbUtils.queryForList(sql, new Object[] {}, User.class);
		System.out.println(list);
	}

	@Test
	public void getListBean1() {
		Map<String, Object> params = new HashMap<>();
		params.put("name", "feny");
		List<User> list = DbUtils.queryForList(params, User.class);
		System.out.println(list);
	}

	@Test
	public void getListForMap() {
		String sql = "SELECT * FROM user WHERE name LIKE CONCAT('%',?,'%') ";
		Object[] params = { "f" };
		List<Map<String, Object>> map = DbUtils.queryForList(sql, params);
		System.out.println(map);
	}

	/**
	 * 事务管理
	 * 
	 * @author Feny
	 * @date 2018年7月24日上午10:15:46
	 */
	@Test
	public void TxTest() {
		DbUtils.useTx(new Tx() {
			@Override
			public void run() {
				User user = new User();
				user.setCreatTime(new Date());
				user.setName(new Date().getTime() + "");
				user.setPassword("12789");
				user.setCash(new BigDecimal("100.123"));
				long id = DbUtils.insert(user);

				Article article = new Article();
				article.setTitle("test title");
				article.setContent("test content");
				article.setId(String.valueOf(id));
				DbUtils.insert(article);
				System.out.println(1 / 0);
			}
		});
	}

}