package examples.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import sim.core.annotation.Uri;
import sim.core.mvc.Model;
import sim.core.mvc.render.Render;

@Uri("test")
public class TestController {

	@Uri(value = "/ioc.do")
	public void ioc(Model model) throws IOException {
		model.renderText("test msg");
	}

	@Uri(value = "renderText")
	public void renderText() throws IOException {
		Render.renderText("test msg");
	}

	@Uri(value = "/renderJsp.do")
	public void renderJsp(Model model) throws IOException {
		model.setAttr("msg", "test renderJsp");
		model.renderJsp("/home.jsp");
	}

	@Uri(value = "/renderForTemp.do")
	public void renderForTemp(Model model) throws IOException {
		model.setAttr("msg", "test msg");
		model.renderFreeMarker("/test.ftl");
	}

	@Uri("/redirect.do")
	public void redirect(Model model, HttpServletRequest request) {
		model.redirect("/renderJsp.do");
	}
}
