package examples.controller;

import examples.model.User;
import examples.service.UserService;
import sim.core.annotation.Param;
import sim.core.annotation.Uri;
import sim.core.dbrecord.Page;
import sim.core.mvc.Model;

@Uri("/user")
public class UserController extends Model {

	private UserService userService = new UserService();

	@Uri("/getList.do")
	public void getList(Page page) {
		page = userService.findForPage(null, null, page);
		renderJson(page);
	}

	@Uri("/getOne.do")
	public void getOne(@Param("id") String id) {
		User user = userService.findById(id);
		renderJson(user);
	}

}
