package examples.controller;

import sim.core.annotation.Uri;
import sim.core.mvc.Model;
import sim.core.mvc.render.Render;

public class LoginController extends Model {

	@Uri("/doLogin.do")
	public void doLogin() {
		String name = getParam("name");
		String password = getParam("password");
		if ("feny".equals(name) && "123456".equals(password)) {
			setAttr("name", name);
			Render.renderJsp("/home.jsp");
			return;
		}
		setAttr("msg", "用户名或密码错误！");
		Render.renderJsp("/login.jsp");
	}
}
