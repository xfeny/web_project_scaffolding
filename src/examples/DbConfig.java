package examples;

import sim.core.dbrecord.DbUtils;
import sim.core.dbrecord.dataSoure.DefaultDataSource;
import sim.core.dbrecord.dataSoure.DruidDataSource;
import sim.core.mvc.Config;
import sim.core.utils.PropUtils;

/**
 * 项目启动时，初始化数据库连接
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月30日下午5:44:00
 */
public class DbConfig implements Config {

	@Override
	public void start() {
		String jdbcUrl = PropUtils.getProp("database.url");
		String user = PropUtils.getProp("database.username");
		String password = PropUtils.getProp("database.password");
		// DefaultDataSource dataSource = new C3p0DataSource(jdbcUrl, user, password);
		DefaultDataSource dataSource = new DruidDataSource(jdbcUrl, user, password);
		dataSource.start();
		DbUtils.init(dataSource.getDataSource());
	}

}
