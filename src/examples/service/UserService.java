package examples.service;

import java.util.List;

import examples.base.BaseService;
import examples.model.User;
import sim.core.dbrecord.Page;

public class UserService extends BaseService<User> {

	public List<User> queryForList() {
		String where = "WHERE name LIKE CONCAT('%',?,'%')";
		Object[] params = { "f" };
		return super.findForList(where, params);
	}

	public Page queryForPage(Page page, String name) {
		String where = "WHERE name LIKE CONCAT('%',?,'%')";
		Object[] params = { name };
		return super.findForPage(where, params, page);
	}
}
