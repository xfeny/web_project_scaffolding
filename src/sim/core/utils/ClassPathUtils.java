package sim.core.utils;

import java.io.File;

public class ClassPathUtils {

	public static File getResourcePath(String path) {
		return new File(new ClassPathUtils().getClass().getResource(path).getPath());
	}

	public static String getRootClassPath() {
		return new File(new ClassPathUtils().getClass().getResource("/").getPath()).getPath();
	}

	public static void main(String[] args) {
		System.out.println(getRootClassPath());
		System.out.println(getResourcePath("/public/test.ftl"));
	}
}
