package sim.core.utils;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ServletContext Attribute
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月26日下午2:02:51
 */
public class ServletContextUtils {

	public static List<String> DEFAULT_IGNORE = new ArrayList<String>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			String[] str = { "jsp", "css", "htm", "html", "bmp", "png", "jpg", "jpeg", "gif", "ico", "txt", "js", "pdf", "ttf", "wof" };
			addAll(Arrays.asList(str));

		}
	};

	/**
	 * 请求地址后缀
	 */
	public static String URI_SUFFIX = "";

	/**
	 * jsp存放路径，默认跟目录
	 */
	public static String JSP_RESOURCES = "/WEB-INF";

	/**
	 * 项目ContextPath
	 */
	public static String CONTEXTPATH = "";

	/**
	 * FreeMarker文件路劲
	 */
	public static String FREEMARKER_RESOURCES = "/public";

	private static ThreadLocal<HttpServletRequest> threadLocalRequest = new ThreadLocal<>();
	private static ThreadLocal<HttpServletResponse> threadLocalResponse = new ThreadLocal<>();

	public static void setReqAndRes(HttpServletRequest request, HttpServletResponse response) {
		threadLocalRequest.set(request);
		threadLocalResponse.set(response);
	}

	public static void removeReqAndRes() {
		threadLocalRequest.remove();
		threadLocalResponse.remove();
	}

	public static HttpServletRequest getRequest() {
		return threadLocalRequest.get();
	}

	public static HttpServletResponse getResponse() {
		return threadLocalResponse.get();
	}

	public static String getContextPath() {
		return getRequest().getContextPath();
	}

	public static String getRemoteAddr() {
		return getRequest().getRemoteAddr();
	}

	public static Writer getWriter() {
		try {
			return getResponse().getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
