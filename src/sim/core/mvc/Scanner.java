package sim.core.mvc;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import sim.core.utils.ClassPathUtils;

/**
 * 注解扫描类
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月25日下午2:27:12
 */
public class Scanner {

	/**
	 * 遍历
	 * 
	 * @author Feny
	 * @date 2018年7月26日下午5:52:16
	 * @param path
	 * @param className
	 * @return
	 */
	public static List<Class<?>> getAllClass(String path, List<Class<?>> className) {
		List<Class<?>> clazzs = new ArrayList<>();

		File[] childFiles = Paths.get(path).toFile().listFiles();
		for (File childFile : childFiles) {
			if (childFile.isDirectory()) {
				clazzs.addAll(getAllClass(childFile.getPath(), clazzs));
			} else {
				String childFilePath = childFile.getPath();
				if (childFilePath.endsWith("class")) {
					childFilePath = childFilePath.substring(childFilePath.indexOf("\\classes") + 9, childFilePath.lastIndexOf("."));
					childFilePath = childFilePath.replace(File.separator, ".");
					try {
						Class<?> clazz = Class.forName(childFilePath);
						clazzs.add(clazz);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return clazzs;
	}

	public static void main(String[] args) {
		String path = ClassPathUtils.getRootClassPath();
		System.out.println(Scanner.getAllClass(path, null));
	}
}
