package sim.core.mvc.render;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

import sim.core.utils.ClassPathUtils;
import sim.core.utils.FreeMarkerUtils;
import sim.core.utils.ServletContextUtils;

public class Render {
	private static HttpServletRequest getRequest() {
		return ServletContextUtils.getRequest();
	}

	private static HttpServletResponse getResponse() {
		return ServletContextUtils.getResponse();
	}

	public static void renderJsp(String view) {
		try {
			HttpServletRequest request = getRequest();
			request.getRequestDispatcher(ServletContextUtils.JSP_RESOURCES + view).forward(request, getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void renderJson(Object data) {
		renderJson(JSONObject.toJSONString(data));
	}

	public static void renderJson(String jsonStr) {
		HttpServletResponse response = getResponse();
		write(jsonStr, response, ContentType.JSON.value());
	}

	public static void renderXml(String xml) {
		HttpServletResponse response = getResponse();
		write(xml, response, ContentType.XML.value());
	}

	public static void renderHtml(String html) {
		HttpServletResponse response = getResponse();
		write(html, response, ContentType.HTML.value());
	}

	public static void renderJavaScript(String javaScript) {
		HttpServletResponse response = getResponse();
		write(javaScript, response, ContentType.JAVASCRIPT.value());
	}

	public static void renderText(String text) {
		HttpServletResponse response = getResponse();
		write(text, response, ContentType.TEXT.value());
	}

	/**
	 * 渲染到FreeMarker模板
	 * 
	 * @author Feny
	 * @date 2018年7月27日下午5:01:07
	 * @param tempPath
	 */
	public static void renderFreeMarker(String tempPath) {
		Map<String, Object> data = new HashMap<>();
		HttpServletRequest request = getRequest();
		HttpServletResponse response = getResponse();
		response.setContentType("text/html; charset=UTF-8");
		for (Enumeration<String> attrs = request.getAttributeNames(); attrs.hasMoreElements();) {
			String attrName = attrs.nextElement();
			data.put(attrName, request.getAttribute(attrName));
		}
		try {
			File path = ClassPathUtils.getResourcePath(ServletContextUtils.FREEMARKER_RESOURCES + tempPath);
			FreeMarkerUtils.writer(path, data, response.getWriter());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void redirect(String redirect) {
		try {
			getResponse().sendRedirect(ServletContextUtils.CONTEXTPATH + redirect);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void write(String data, HttpServletResponse response, String contentType) {
		try {
			response.setContentType(contentType);
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
