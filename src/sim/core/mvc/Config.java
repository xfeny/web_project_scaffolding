package sim.core.mvc;

/**
 * 启动时初始化数据，实现该接口<br>
 * 例如：数据库连接初始化
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月30日下午2:26:11
 */
public interface Config {
	public void start();
}
