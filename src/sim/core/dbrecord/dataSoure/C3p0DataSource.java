package sim.core.dbrecord.dataSoure;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class C3p0DataSource extends DefaultDataSource {

	private int maxPoolSize = 100;
	private int minPoolSize = 10;
	private int initialPoolSize = 10;
	private int maxIdleTime = 20;
	private ComboPooledDataSource dataSource;

	public C3p0DataSource() {
		super();
	}

	public C3p0DataSource(String jdbcUrl, String user, String password) {
		super(jdbcUrl, user, password);
	}

	public C3p0DataSource(String jdbcUrl, String user, String password, String driverClass) {
		super(jdbcUrl, user, password, driverClass);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void start() {
		try {
			dataSource = new ComboPooledDataSource();// 创建连接池实例
			dataSource.setDriverClass(this.driverClass);// 设置连接池连接数据库所需的驱动
			dataSource.setJdbcUrl(this.jdbcUrl);// 设置连接数据库的URL
			dataSource.setUser(this.user);// 设置连接数据库的用户名
			dataSource.setPassword(this.password);// 设置连接数据库的密码
			// 设置连接池的最大连接数
			dataSource.setMaxPoolSize(this.maxPoolSize);
			// 设置连接池的最小连接数
			dataSource.setMinPoolSize(this.minPoolSize);
			// 设置连接池的初始连接数
			dataSource.setInitialPoolSize(this.initialPoolSize);
			dataSource.setMaxIdleTime(this.maxIdleTime);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}

	public int getMaxPoolSize() {
		return maxPoolSize;
	}

	public void setMaxPoolSize(int maxPoolSize) {
		if (maxIdleTime < 1)
			throw new IllegalArgumentException("maxPoolSize must more than 0.");
		this.maxPoolSize = maxPoolSize;
	}

	public int getMinPoolSize() {
		return minPoolSize;
	}

	public void setMinPoolSize(int minPoolSize) {
		if (maxIdleTime < 1)
			throw new IllegalArgumentException("minPoolSize must more than 0.");
		this.minPoolSize = minPoolSize;
	}

	public int getInitialPoolSize() {
		return initialPoolSize;
	}

	public void setInitialPoolSize(int initialPoolSize) {
		if (maxIdleTime < 1)
			throw new IllegalArgumentException("initialPoolSize must more than 0.");
		this.initialPoolSize = initialPoolSize;
	}

	public int getMaxIdleTime() {
		return maxIdleTime;
	}

	public void setMaxIdleTime(int maxIdleTime) {
		if (maxIdleTime < 1)
			throw new IllegalArgumentException("maxIdleTime must more than 0.");
		this.maxIdleTime = maxIdleTime;
	}

}
