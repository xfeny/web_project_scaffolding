package sim.core.dbrecord.dataSoure;

import java.sql.SQLException;
import javax.sql.DataSource;

public class DruidDataSource extends DefaultDataSource {

	// <!-- 配置大小、最大、最小 、超时时间-->
	private int maxActive = 100;
	private int initialSize = 1;
	private int maxWait = 3600000;
	private int minIdle = 30;
	private String filters = "wall,stat";

	private com.alibaba.druid.pool.DruidDataSource dataSource;

	public DruidDataSource() {
		super();
	}

	public DruidDataSource(String jdbcUrl, String user, String password, String driverClass) {
		super(jdbcUrl, user, password, driverClass);
	}

	public DruidDataSource(String jdbcUrl, String user, String password) {
		super(jdbcUrl, user, password);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void start() {
		dataSource = new com.alibaba.druid.pool.DruidDataSource();// 创建连接池实例
		dataSource.setUrl(this.jdbcUrl);// 设置连接数据库的URL
		dataSource.setUsername(this.user);// 设置连接数据库的用户名
		dataSource.setPassword(this.password);// 设置连接数据库的密码
		dataSource.setMaxActive(this.maxActive);
		dataSource.setInitialSize(this.initialSize);
		dataSource.setMaxWait(this.maxWait);
		dataSource.setMinIdle(this.minIdle);
		try {
			dataSource.setFilters(this.filters);
		} catch (SQLException e) {
		}
	}

	public int getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}

	public int getInitialSize() {
		return initialSize;
	}

	public void setInitialSize(int initialSize) {
		this.initialSize = initialSize;
	}

	public int getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}

	public int getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public void setDataSource(com.alibaba.druid.pool.DruidDataSource dataSource) {
		this.dataSource = dataSource;
	}

}
