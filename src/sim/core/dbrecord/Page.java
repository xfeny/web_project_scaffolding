package sim.core.dbrecord;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 分页对象. 包含当前页数据及分页信息如总记录数.
 * 
 * @author 许芬勇
 * @Date 2015-11-9 10:32
 */
@SuppressWarnings({ "unused", "serial" })
public class Page implements Serializable {

	private static int DEFAULT_PAGE_SIZE = 20;

	private long pageSize = DEFAULT_PAGE_SIZE; // 每页的记录数

	private Object data = new ArrayList<>(DEFAULT_PAGE_SIZE); // 当前页中存放的记录,类型一般为List

	private long totalCount; // 总记录数
	private long totalPage;// 总页数
	private long page = 1;// 当前页

	/**
	 * 默认构造方法.
	 */
	public Page() {
	}

	public Page(long currentPage, long totalSize, long pageSize) {
		this.pageSize = pageSize;
		this.totalCount = totalSize;
		this.page = currentPage;
	}

	/**
	 * 构造方法
	 * 
	 * @param currentPage当前页
	 * @param pageSize每页的行数
	 * @param data本页包含的数据
	 */
	public Page(long currentPage, long totalSize, long pageSize, Object data) {
		this.pageSize = pageSize;
		this.totalCount = totalSize;
		this.data = data;
		this.page = currentPage;
	}

	/**
	 * 取总记录数.
	 */
	public long getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * 取每页数据容量.
	 */
	public long getPageSize() {
		return pageSize;
	}

	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 取该页当前页码,页码从1开始.
	 */
	public long getPage() {
		return this.page;
	}

	/**
	 * 该页是否有下一页.
	 */
	public boolean hasNextPage() {
		return this.getPage() < this.getTotalPage() - 1;
	}

	/**
	 * 该页是否有上一页.
	 */
	public boolean hasPreviousPage() {
		return this.getPage() > 1;
	}

	public void setPage(long page) {
		this.page = page < 1 ? 1 : page;
	}

	/**
	 * 取当前页中的记录.
	 */
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 取总页数.
	 */
	public long getTotalPage() {
		if (totalCount % pageSize == 0)
			return totalCount / pageSize;
		else
			return totalCount / pageSize + 1;
	}
}