package sim.core.dbrecord;

/**
 * 事务
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月24日上午10:27:07
 */
public interface Tx {
	public void run();
}
