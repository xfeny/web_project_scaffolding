package sim.core.handler;

import java.util.ArrayList;
import java.util.List;

import sim.core.consts.RequestMethod;

public class RequestMethodHandler {
	public static List<String> getRequestMethod(RequestMethod[] requestMethods) {
		List<String> list = new ArrayList<>();
		for (RequestMethod requestMethod : requestMethods) {
			list.add(requestMethod.name());
		}
		return list;
	}
}
