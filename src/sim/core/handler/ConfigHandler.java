package sim.core.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import sim.core.mvc.Config;

/**
 * Config执行类
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月30日下午1:55:31
 */
public class ConfigHandler {
	static List<Class<?>> configs = new ArrayList<>();

	public ConfigHandler getConfigClass(List<Class<?>> clazzs) {
		for (Class<?> clazz : clazzs) {
			Class<?>[] superC = clazz.getInterfaces();
			List<Class<?>> list = Arrays.asList(superC);
			if (list.contains(Config.class)) {
				configs.add(clazz);
			}
		}
		return this;
	}

	/**
	 * 执行Config的start方法
	 * 
	 * @author Feny
	 * @date 2018年7月30日下午1:58:54
	 */
	public ConfigHandler startConfig() {
		for (Class<?> clazz : configs) {
			try {
				Config config = (Config) clazz.newInstance();
				config.start();
				System.out.printf("Config Start：【%s】 Successful\n", clazz.getName());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return this;
	}
}
