package sim.core.consts;

public enum CompareEnum {
	EQ("="), NEQ("<>"), GT(">"), GTE(">="), LT("<"), LTE("<="), LIKE("LIKE"), IN("IN"), NOT_IN("NOT IN"), BETWEEN("BETWEEN");
	private String value;

	private CompareEnum(String value) {
		this.value = value;
	}

	public String getVal() {
		return value;
	}
}
