package sim.core.consts;

/**
 * properties配置的key
 * 
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月27日下午2:05:10
 */
public class PropertiesConst {
	/**
	 * 请求地址后缀
	 */
	public static final String PROP_REQUEST_SUFFIX = "resources.request.suffix";

	/**
	 * jsp文件存放地址
	 */
	public static final String PROP_JSP = "resources.jsp";

	/**
	 * freemarker文件存放目录
	 */
	public static final String PROP_FREEMARKER = "resources.freemarker";
}
