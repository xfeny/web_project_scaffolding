package sim.core.consts;

/**
 * @author Feny
 * @email 1220301855@qq.com
 * @version 1.0
 * @date 2018年7月27日上午9:03:27
 */
public enum RequestMethod {
	GET, POST;

}
